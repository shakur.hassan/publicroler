require_dependency "polku_roler/application_controller"

module PolkuRoler
  class RolesController < ApplicationController
    #skip_before_action :verify_authenticity_token

    def index
      @notice = params[:notice]
      if !session[:token]
        logout
        redirect_to '/auth/comille', notice: 'Login session is expired, please login again'
      elsif !current_user_admin?
        @notice = 'Only for admins'
      else
        api_request = auth_api_service.get_users
        @api_status = api_request.status
        if @api_status == 401
          logout
          redirect_to '/auth/comille', notice: 'Login session is expired, please login again'
        end
        api_result = api_request.body
        @users = []
        @roles = []
        unless api_result["data"].nil?
          @users = api_result["data"].map do |user|
            user = user["attributes"].stringify_keys
            @roles |= user['roles'].split(',').each(&:strip) unless user['roles'].nil?
            User.new(id: user['id'], name: user['full_name'], roles: user['roles'], email: user['email'])
          end
          @users.sort_by!(&:id)
          @roles.sort!
        end
      end
    end

    def logout
      session.delete(:current_user)
      session.delete(:token)
      session.delete(:token_expires_at)
      session.delete(:refresh_token)
    end

    def get_user_token
      @token ||= session[:token]
    end

    def current_user_admin?
      token = get_user_token
      if token.nil?
        return false
      end
      jwt = JWT.decode(token, OpenSSL::PKey::RSA.new(Rails.application.credentials.jwt_public_key), true, {algorithm: 'RS256'})
      if jwt.empty?
        return false
        end
      if jwt[0]&.try(:[], 'user').try(:[], 'business_roles')&.include?('admin')
        return true
      end
      false
    end

    def new_user
      puts "Invite user, params: #{params}"
      resp = auth_api_service.invite_user(
          get_user_token,
          '',
          params[:email],
          params[:email].split('@')[0],
          params[:roles],
          ''
      )
      Rails.logger.info "api result:  #{resp}"
      notice = status_message(resp, "Invite user: #{params[:email]} failed ")
      redirect_to action: 'index', notice: notice

    end

    def auth_api_service
      @user_api ||= UserApi.new({token: get_user_token})
    end

    def status_message(response, prefix)
      begin
        if !response.status.to_i.between?(200, 399)
          prefix + Net::HTTPResponse::CODE_TO_OBJ[r.status.to_s].to_s
        else
          ''
        end
      rescue Exception
        ''
      end
    end

    def toggle_role
      Rails.logger.info "toggle_role #{params[:user_id]}, #{params[:role]}, #{params[:method]}"
      user_id = params[:user_id]
      role = params[:role]
      method = params[:method] # remove / add
      roles = params[:roles]
      if user_id.present? && role.present?
        current_roles = roles.present? ? roles.split(',').map!(&:strip) : []
        p current_roles
        if method == 'remove'
          current_roles.delete(role)
          roles = current_roles
        else
          roles = current_roles | [role]
        end
        roles.map!(&:strip)
        update_role_params = { roles: roles.join(',') }
        api_request = auth_api_service.update_user(user_id, update_role_params)
      end

      notice = status_message(api_request, "Request #{method} role: #{role}, at user: #{user_id} failed ")
      redirect_to action: 'index', notice: notice
    end
  end
end
