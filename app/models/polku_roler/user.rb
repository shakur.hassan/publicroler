module PolkuRoler
  class User
    include ActiveModel::Model

    attr_accessor :id, :name, :roles, :email

    def self.get_users
      users = dummy_users
      p users
      users.map do |user|
        user = user.stringify_keys
        User.new(id: user['id'], name: user['full_name'], roles: user['roles'], email: user['email'])
      end
    end

    def self.dummy_users
      [{"id":"c2b4a887-5d54-4dbb-a5cf-8068f287a899","username":nil,"full_name":"kantaree lievetmursu","phone":"","email":"kantareeli@comille.fi","tag_ids":[],"roles":"admin, member, special","tags":[],"events":[],"data_blocks":[]},
       {"id":"bd4f4e57-162a-4f1b-8ae0-7f7592ae9af6","username":nil,"full_name":"kan","phone":"","email":"kan@robot.com","tag_ids":[],"roles":"admin, member","tags":[],"events":[],"data_blocks":[]},
       {"id":"bd4f4e57-162a-4f1b-8ae0-7f7592ae9af5","username":nil,"full_name":"kan2","phone":"","email":"kan2@robot.com","tag_ids":[],"roles":"member","tags":[],"events":[],"data_blocks":[]}]
    end

    def roles_as_array
      roles = []
      return roles if self.roles.nil?

      self.roles.split(',').each{|m| roles << m.strip}
      roles.flatten.uniq
    end
  end
end
