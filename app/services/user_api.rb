
class UserApi
  require 'jsonclient'
  require 'optparse'
  require 'httplog' if Rails.env.development?

  HttpLog.configure do |config|
    config.log_headers = true
  end

  def initialize(params)
    @token = params[:token]
    @base_url = ENV['AUTH_API_URL']
    @json_client = JSONClient.new({base_url: @base_url})
    @invite_user_path = "api/v1/users/invite"
  end

  def invite_user(client_token, phone, email, name, roles, message)

    puts "token #{client_token}"
    params = {
        phone: phone,
        full_name: name,
        email: email,
        roles: roles,
        message: message
    }

    response = @json_client.post(@invite_user_path, params, {"Authorization"=>"Bearer #{client_token}"})
    response.body if response.present?
  end

  def get_users(params = {})
    p @token
    @json_client.get("/api/v1/users/app_users?#{params.to_query}", { options: { include: [
        "tags","tags.name", "events", "events.name", "events.occurred_at", "data_blocks.name"]}}.to_query,
              {"Authorization" => "Bearer #{@token}"})
  end

  def get_user(user_id)
    @json_client.get( "/api/v1/users/#{user_id}", { options: {include: ["tags","tags.name"]}}.to_query,
               {"Authorization" => "Bearer #{@token}"})
  end

  # def update_user(user_id, params)
  #   @json_client.put(
  #       "/api/v1/users/#{user_id}",
  #       {user:params}, {"Authorization" => "Bearer #{@token}"})
  # end

  def update_user(user_id, params)
    @json_client.post(
        "/api/v1/users/app_user_update/#{user_id}",
        {user:params}, {"Authorization" => "Bearer #{@token}"})
  end

end