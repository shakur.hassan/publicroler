PolkuRoler::Engine.routes.draw do
  root to: 'roles#index'
  post 'toggle_role', to: 'roles#toggle_role'
  post 'new_user', to: 'roles#new_user'

end
