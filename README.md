# PolkuRoler
UI of user management for polku-app which is get authentication by Auth-api


## Installation
## Step 1. Add gem line to your application's Gemfile:

```ruby
gem 'slim-rails'   # if not exist 
gem 'httpclient'   # if not exist
gem 'httplog'  # if not exist
# 
For Develop
gem 'polku_roler', path: '../polku_roler2'  # path of this gem in local

# for production 
#TODO : test this gem in git
gem 'polku_roler', git: 'https://gitlab.com/comille-alusta/polku_roler.git'
```

## Step 2. And then execute:
```bash
$ bundle
```

## Step 3. Add css and js for gem to project:

```javascript
# app/assets/config/manifest.js

//= link polku_roler/application.css
//= link polku_roler/application.js
```

## Step 4. Add session[:token] variable to project 
At the place that get token from authen-api:

```ruby
# (for example)   app/controllers/sessions_controller.rb

 session[:token] = auth_hash['credentials']['token']

```


## Step 5. Mount engine to the main app:
```ruby
# config/routes.rb

Rails.application.routes.draw do
  # app routes
  
  #
  mount PolkuRoler::Engine => "/polku_roler", :as => "polku_roler"

end

```

Engine's pages will have URL prefix 'polku_roler/'. For example, URL for articles page from Engine:

example 
http://localhost:3000/polku_roler





## Step 6. Add menu to access engine's route 
use the name specified in mount in 'as':  mount PolkuRoler::Engine => "/polku_roler", :as => "myengine"

```ruby
# (for example) app/views/layouts/application.html.erb
 
= link_to 'User Management', polku_roler.root_path, target: '_blank
```








